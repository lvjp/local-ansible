#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail
set -o xtrace

echo "Prepare environment for ansible"

sudo dnf install ansible python3-ansible-lint

cd "$(mktemp -d)"

git clone https://gitlab.com/lvjp/local-ansible.git
cd local-ansible
make ssh
make
