all:
	ansible-playbook \
		--diff \
		--inventory hosts.yml \
		--ask-become-pass \
		--skip-tags git.clone \
		--skip-tags ssh.keys \
		workstation.yml

check:
	ansible-playbook \
		--check \
		--diff \
		--inventory hosts.yml \
		--ask-become-pass \
		--skip-tags git.clone \
		--skip-tags ssh.keys \
		workstation.yml

ssh:
	ansible-playbook  \
		--inventory hosts.yml \
		--tags ssh.keys \
		workstation.yml

git:
	ansible-playbook  \
		--inventory hosts.yml \
		--tags git.clone \
		workstation.yml

local-pipeline: ansible-lint super-linter

ansible-lint:
	docker run \
		--rm \
		--volume "$(shell pwd):/work:z" \
		--workdir /work \
		--entrypoint "" \
		github/super-linter:v4.9.7 ansible-lint

super-linter:
	docker run \
		--rm \
		--volume "$(shell pwd):/work:z" \
		--env RUN_LOCAL=true \
		--env DEFAULT_WORKSPACE=/work \
		github/super-linter:v4.9.7 bash

.PHONY: all ansible-lint check git local-pipeline ssh super-linter
